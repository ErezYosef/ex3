#define _CRT_SECURE_NO_WARNINGS 


#include "general.h"





/*
int append_new_string_to_file(char line[])
{
	FILE* file_pointer = fopen("Computation.txt", "a");
	if (file_pointer == NULL)
	{
		printf("FATAL ERROR- Couldn't open file- Exiting...");
		return -1;
	}
	fprintf(file_pointer, "%s\n", line);
	fclose(file_pointer);
	return 0;
}
*/

/*
// Write_arr_to_file:	write arrays as needed to fname
//input arguments :	fname, arr1, arr2, arr_len
// result argument:	RET_SUCCESS / RET_FAIL
int Write_arr_to_file(char fname[], int arr1[], int arr2[], int arr_len)
{
	int i = 0;
	FILE *file_pointer = NULL;
	file_pointer = fopen(fname, "w");
	if (file_pointer == NULL)
	{
		printf("fatal error couldn't open file\n"); //Error opening file exit with code 1
		return RET_FAIL;
	}
	for (i = 0; i < arr_len; i++)
	{
		fprintf(file_pointer, "%d %d", arr1[i], arr2[i]);
		if (i < arr_len - 1)
			fprintf(file_pointer, "\n");
	}
	

	fclose(file_pointer);
	return RET_SUCCESS;
}*/


// get_first_word_in_line1: 	break the first word of the line to seperate string
//input arguments :		line string, word string to get first word
// result argument:		pointer to the rest of  the string
char* get_first_word_in_line(char line[], char word[])
{
	int start = 0;
	int end = 0;

	while (line[start] == ' ') 
	{
		start++;
	} // start on first letter
	end = start;
	while (!((line[end] == ' ')  || (line[end] == '\n') || (line[end] == 0)))
	{
		end++;
	}
	line[end] = 0; //end of word
	strcpy(word, line + start);
	return (line + end + 1);
}


int Override_existing_file(char path[])
{
	char path_copy[MAX_COMMAND_LINE];
	FILE *file_pointer = NULL;
	strcpy(path_copy, path);
	strcat(path_copy, "/roomLog.txt");
	file_pointer = fopen(path_copy, "w");
	if (file_pointer == NULL)
	{
		printf("fatal error couldn't open file\n"); //Error opening file exit with code 1
		return RET_FAIL;
	}


	fclose(file_pointer);
	return RET_SUCCESS;
}