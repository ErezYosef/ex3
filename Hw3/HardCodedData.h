#pragma once
#define MAX_STR_LEN 21
#define MAX_FILE_LINE_LEN 40
#define MAX_ROOMS 5
#define MAX_VISITORS 15

//#define MAX 1
#define MAX_COMMAND_LINE 300

#define RET_SUCCESS 0
#define RET_FAIL 1
#define RET_FAIL_NOVALUE -1



typedef struct _DATAHOTEL {
	char path[MAX_COMMAND_LINE];
	int current_day;

	char room_name[MAX_ROOMS][MAX_STR_LEN];
	int room_price[MAX_ROOMS];
	int room_available[MAX_ROOMS];

	char visitor_name[MAX_VISITORS][MAX_STR_LEN];
	int visitor_money[MAX_VISITORS];
	
} DATAHOTEL;

typedef struct _Thread_param {
	DATAHOTEL *data_ptr;
	int ID;

} Thread_param;