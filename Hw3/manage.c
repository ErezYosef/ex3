#define _CRT_SECURE_NO_WARNINGS 

#include "manage.h"

// manage_flow : get the path to files and manage the flow of the program
// Input : files path
// Returns : RET_SUCCESS / RET_FAIL
int manage_flow(char file_path[])
{
	int  visitor=0;
	int opened_threads;
	int visitors_count = 0;
	int wait_code = 0;
	DWORD p_thread_ids[MAX_VISITORS];
	HANDLE threads_handles[MAX_VISITORS];
	
	// DATA:
	DATAHOTEL data_hotel;
	DATAHOTEL *data_hotel_ptr = &data_hotel;
	Thread_param thread_param[MAX_VISITORS];
	

	// init data_hotel struct:
	visitors_count = read_files_to_datahotel(data_hotel_ptr, file_path);
	if (visitors_count == RET_FAIL)
		return RET_FAIL;
	strcpy(data_hotel_ptr->path, file_path);
	data_hotel_ptr->current_day = 0;

	// init thread_param array for each visitor:
	for (visitor = 0; visitor < MAX_VISITORS; visitor++)
	{
		thread_param[visitor].data_ptr = data_hotel_ptr;
		thread_param[visitor].ID = visitor;
	}

	// override existing output file:
	if (Override_existing_file(file_path) == RET_FAIL)
		return RET_FAIL;

	// debugging
	for (int i = 0; i <4; i++)
	{
		printf("%s, %d %d\n",  data_hotel.room_name[i], data_hotel.room_price[i] , data_hotel.room_available[i] );
		
	}

	// open threads:
	opened_threads = manage_open_threads(visitors_count, threads_handles, p_thread_ids, thread_param);
	if (opened_threads != visitors_count)
	{// ERROR in open processes - not all processes opened
		printf("Thread Creation faild on number %d\n", opened_threads);
		visitors_count = opened_threads;
		manage_close_threads(visitors_count, threads_handles);
		return RET_FAIL;
	}

	while (1)
	{
		// check if all threads are done - with no money
		wait_code = WaitForMultipleObjects(visitors_count, threads_handles, TRUE, 20);
		if (wait_code == WAIT_OBJECT_0)
		{ // all threads are done
			break;
		}
		if (wait_code != WAIT_TIMEOUT)
		{
			printf("Error in wait for threads\n");
			manage_close_threads(visitors_count, threads_handles);
			return RET_FAIL;
		}

		// wait for threads to done the work
		// get signal to change day (all threads are waiting)
		data_hotel_ptr->current_day += 1;
		// give signal to start: CHECKOUT -> CHECKIN
		// ...

		
	}


	int close_stasus = manage_close_threads(visitors_count, threads_handles); //?
	return RET_SUCCESS;

}




// manage_close_processes : close the handles of the processes and get the exit code to array
// Input : ids_count, process_info, process_exit_code
// Returns : RET_SUCCESS / RET_FAIL
int manage_close_threads(int count, HANDLE handles[])
{
	int i = 0;
	int ret_val;
	int err_exitcode = RET_SUCCESS;
	for (i = 0; i < count; i++)
	{
		ret_val = CloseHandle(handles[i]);
		if (0 == ret_val) 
		{
			printf("Error when closing\n");
			err_exitcode = RET_FAIL;
		}

	}
	return err_exitcode;
}

// manage_open_processes : create the processes and put their info in the arrays
// Input : ids_count, process_handles, process_info, file_path, ids
// Returns : The amount of processes that opened
int manage_open_threads(int count, HANDLE handles[], DWORD p_thread_ids[] , Thread_param thread_param[])
{
	int i = 0;
	
	for (i = 0; i < count; i++)
	{
		handles[i] = CreateThreadSimple(threadfunc, &thread_param[i], &p_thread_ids[i] );
		if (handles[i] == NULL)
			return i;
	}
	return i;
}


// read_files_to_datahotel: 	break the operation line to parts (words)
//input arguments :		line string, string array to words of the operation
// result argument:		indication 0 for exit success
int read_files_to_datahotel(DATAHOTEL *data, char path[])
{
	char path_copy[MAX_COMMAND_LINE];
	char cur_line[MAX_FILE_LINE_LEN];

	int arr_position = 0, cur_line_len = 0;
	int line_value1 = 0, line_value2 = 0, line_value3 = 0;
	char *rest_line;
	char line_copy[MAX_FILE_LINE_LEN];
	char line_component1[MAX_STR_LEN];
	char line_component2[MAX_STR_LEN];
	char line_component3[MAX_STR_LEN];
	// -------
	// Read Rooms:
	strcpy(path_copy, path);
	strcat(path_copy, "/rooms.txt");
	FILE* file_pointer = fopen(path_copy, "r");
	if (file_pointer == NULL)
	{
		printf("Error: opening %s failed\n", path_copy);
		return RET_FAIL; 
	}
	
	while (fgets(cur_line, MAX_FILE_LINE_LEN, file_pointer) != NULL)
	{
		cur_line_len = strlen(cur_line);
		strcpy(line_copy, cur_line);

		rest_line = get_first_word_in_line(line_copy, line_component1);
		if (rest_line >= line_copy + cur_line_len)
			return RET_FAIL; 
		rest_line = get_first_word_in_line(rest_line, line_component2);
		if (rest_line >= line_copy + cur_line_len)
			return RET_FAIL;
		rest_line = get_first_word_in_line(rest_line, line_component3);


		line_value2 = (int)strtol(line_component2, NULL, 10);
		line_value3 = (int)strtol(line_component3, NULL, 10);
		
		strcpy(data->room_name[arr_position], line_component1);
		data->room_price[arr_position] = line_value2;
		data->room_available[arr_position] = line_value3;
		
		arr_position++;

		if (arr_position == MAX_ROOMS)
			break;
		
	}

	fclose(file_pointer);
	arr_position = 0;
	// -------
	// Read Visitors names:
	strcpy(path_copy, path);
	strcat(path_copy, "/names.txt");
	file_pointer = fopen(path_copy, "r");
	if (file_pointer == NULL)
	{
		printf("Error: opening %s failed\n", path_copy);
		return RET_FAIL; 
	}
	

	while (fgets(cur_line, MAX_FILE_LINE_LEN, file_pointer) != NULL)
	{
		cur_line_len = strlen(cur_line);
		strcpy(line_copy, cur_line);

		rest_line = get_first_word_in_line(line_copy, line_component1);
		if (rest_line >= line_copy + cur_line_len)
			return RET_FAIL; 
		rest_line = get_first_word_in_line(rest_line, line_component2);


		line_value2 = (int)strtol(line_component2, NULL, 10);

		strcpy(data->visitor_name[arr_position], line_component1);
		data->visitor_money[arr_position] = line_value2;
		arr_position++;

		if (arr_position == MAX_VISITORS)
			break;

	}

	fclose(file_pointer);
	return arr_position;






	

}

