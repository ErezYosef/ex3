#pragma once

#include <stdio.h>
#include <string.h> 

#include <stdlib.h>
#include "HardCodedData.h"
#include "create_process.h"
#include "general.h"
#include "thread_function.h"

//#include <ctype.h>

int manage_flow(char file_path[]);

int manage_close_threads(int count, HANDLE handles[]);

int manage_open_threads(int count, HANDLE handles[], DWORD p_thread_ids[], Thread_param thread_param[]);


int read_files_to_datahotel(DATAHOTEL * data, char path[]);
