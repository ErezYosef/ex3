#include "creat_thread.h"

// create_thread : create all threades and sending the, the relevant parameters
// Input : (the thread's function,parameters for the thread,thread_id)
// Returns : FAIL :NULL , SUCCESS: thread_handle

HANDLE CreateThreadSimple(LPTHREAD_START_ROUTINE p_start_routine, LPVOID p_thread_parameters,LPDWORD p_thread_id)
{
	HANDLE thread_handle;

	if (NULL == p_start_routine)
	{
		printf("Error when creating a thread\n");
		printf("Received LPTHREAD_START_ROUTINE null pointer\n");
		return NULL;
	}

	if (NULL == p_thread_id)
	{
		printf("Error when creating a thread\n");
		printf("Received p_thread_id null pointer\n");
		return NULL;
	}

	thread_handle = CreateThread(
		NULL,            /*  default security attributes */
		0,               /*  use default stack size */
		p_start_routine, /*  thread function */
		p_thread_parameters,            /*  argument to thread function */
		0,               /*  use default creation flags */
		p_thread_id);    /*  returns the thread identifier */

	if (NULL == thread_handle)
	{
		printf("Couldn't create thread\n");
		return NULL;
	}

	return thread_handle;
}




