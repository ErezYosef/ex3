//Authors � Erez Yosef 308017839 ; Shay Shomer Chai 204195663 ;
//Project � Ex2; Introduction to Programming Systems Course;
//Description � ?

#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <string.h> 
#include <ctype.h>
#include <stdlib.h>
#include "HardCodedData.h"
#include "general.h"
#include "thread_function.h"

// main : getting the path to files and write output file
// Input : int argc, char *argv[]
// Returns : 0 when all process made properly and no error accord -- RET_SUCCESS / RET_FAIL
int main(int argc, char* argv[])
{
	int status = 0;
	if (argc != 2)
	{
		printf("Error in input -- argc != 2\n");
		return RET_FAIL;
	}
	status = manage_flow(argv[1]);

	if (status == RET_FAIL)
		return RET_FAIL;
	printf("retsuccrss");
	return RET_SUCCESS;
	
}