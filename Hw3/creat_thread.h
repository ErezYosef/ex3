#define _CRT_SECURE_NO_WARNINGS 
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include "HardCodedData.h"

HANDLE CreateThreadSimple(LPTHREAD_START_ROUTINE p_start_routine, LPVOID p_thread_parameters,LPDWORD p_thread_id);
